export default {
	meta: {
		type: 'problem',
		fixable: false,
		schema: [],
	},
	create(context) {
		const blacklistedIdentifiers = ['xdescribe', 'xit', 'fdescribe', 'fit'];

		return {
			ExpressionStatement(node) {
				if (node.expression.type !== 'CallExpression' || node.expression.callee.type !== 'Identifier') return;
				const identifier = node.expression.callee.name;

				if (!blacklistedIdentifiers.includes(identifier)) return;

				context.report({
					node: node.expression.callee,
					message: `Try to replace this call to "${identifier}" with "${identifier.substring(1)}".`,
				});
			},
		};
	},
};
