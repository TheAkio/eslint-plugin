/* eslint-disable no-console */
/**
 * ESLint rule to respect the configured line length for import statements.
 * Hopefully this lands inside eslint-plugin-import some day (See https://github.com/benmosher/eslint-plugin-import/issues/344)
 *
 * Code inspired by https://github.com/gmsorrow/eslint-plugin-modules-newline/tree/55092f76098d1c78788c9d50886eb5497ed5f7f4
 */
export default {
	meta: {
		type: 'layout',
		fixable: true,
		schema: [
			{
				enum: ['as-needed'],
			},
			{
				type: 'object',
				properties: {
					maxLengthSingelineImport: {
						type: 'integer',
						minimum: 0,
					},
				},
				additionalProperties: false,
			},
		],
	},
	create(context) {
		const logPrefix = '@akio/import-declaration-multiline:';
		const mode = context.options[0] || 'as-needed';

		const opt = {
			maxLengthSingelineImport: 80,
			...context.options[1],
		};

		const maxLengthSingelineImport = opt.maxLengthSingelineImport;

		// Options for singleline length calculation
		const emptyObjectSpacing = true;
		const bracketSize = ' }'.length;
		const commaSize = ', '.length;

		function calculateSinglelineLength(node) {
			const sourceCode = context.getSourceCode();
			if (node.type === 'ImportDeclaration') {
				return sourceCode.getTokens(node).reduce((acc, token) => {
					switch (token.type) {
						case 'Keyword': // Expecting only "import"
							return acc + (token.value.length + 1); // + 1 for space after keyword
						case 'Punctuator': // Expecting "*", "{", "}", "," and ";"
							switch (token.value) {
								case '{':
								case '}':
									if (sourceCode.getTokenAfter(token).value === '}') {
										return acc + (emptyObjectSpacing ? 2 : 1);
									} else if (sourceCode.getTokenBefore(token).value === '{') {
										return acc + 1;
									}
									return acc + bracketSize;
								case '*':
								case ';':
									return acc + 1;
								case ',':
									// We assume there is no singleline trailing comma here
									return acc + commaSize;
								default:
									console.warn(`${logPrefix} Unexpected punctuator value: ${token.value}`);
									return acc;
							}
						case 'Identifier': // Expecting "as", "from"
							if (token.value === 'as' || token.value === 'from') {
								return acc + (token.value.length + 2);
							}
						// Fallthrough for all other identifiers
						case 'String': // Expecting only the module location
							return acc + token.value.length;
						default:
							console.warn(`${logPrefix} Unexpected import declaration token type: ${token.type}`);
							return acc;
					}
				}, 0);
			}

			console.warn(`${logPrefix} Invalid node type for checking line length: ${node.type}`);
		}

		function isRegularImportSpecifier(specifier) {
			return specifier.type === 'ImportSpecifier';
		}

		function isProperlyMultilined(node) {
			const imports = node.specifiers;
			// Assume that one import is always properly multilined
			if (imports.length < 2) return true;

			let foundTwoImportsOnSameLine = false;
			for (let i = 1; i < imports.length; i++) {
				// Ignore default & star import on same line (for now)
				if (!isRegularImportSpecifier(imports[i]) && !isRegularImportSpecifier(imports[i - 1])) continue;

				if (imports[i].loc.start.line === imports[i - 1].loc.start.line) {
					foundTwoImportsOnSameLine = true;
					break;
				}
			}

			return !foundTwoImportsOnSameLine;
		}

		function fixerMultiline(node, fixer) {
			const imports = node.specifiers;
			const sourceCode = context.getSourceCode();
			const fixers = [];

			for (let i = 0; i < imports.length; i++) {
				if (isRegularImportSpecifier(imports[i])) {
					const importIndentifier = sourceCode.getFirstToken(imports[i]);
					const commaBeforeOrOpeningBrace = sourceCode.getTokenBefore(importIndentifier);

					fixers.push(fixer.replaceTextRange(
						[commaBeforeOrOpeningBrace.range[1], importIndentifier.range[0]],
						'\n',
					));
				}
			}

			const lastImport = imports[imports.length - 1];
			let closingBracket = sourceCode.getTokenAfter(lastImport);
			let replaceWith = '\n';
			// We want the last bracket but there might be a trailing comma so we need to check for that
			if (closingBracket.value === ',') {
				closingBracket = sourceCode.getTokenAfter(closingBracket);
				replaceWith = ',\n';
			}

			fixers.push(fixer.replaceTextRange(
				[lastImport.range[1], closingBracket.range[0]],
				replaceWith,
			));

			return fixers;
		}

		function fixerSingleline(node, fixer) {
			const sourceCode = context.getSourceCode();
			const fixers = [];

			fixers.push(fixer.replaceText(
				node,
				sourceCode.getText(node).replace(/\n/g, ''),
			));

			return fixers;
		}

		return {
			ImportDeclaration(node) {
				const isOneLineImport = node.loc.start.line === node.loc.end.line;

				switch (mode) {
					case 'as-needed': {
						const singleLineImportLength = calculateSinglelineLength(node);

						if (singleLineImportLength > maxLengthSingelineImport && isOneLineImport) {
							context.report({
								node,
								message: `This line is longer than ${maxLengthSingelineImport} characters and should be a multiline import`,
								fix(fixer) {
									return fixerMultiline(node, fixer);
								},
							});
						} else if (!isOneLineImport) {
							if (singleLineImportLength <= maxLengthSingelineImport) {
								context.report({
									node,
									message: 'This import should be singleline',
									fix(fixer) {
										return fixerSingleline(node, fixer);
									},
								});
							} else if (!isProperlyMultilined(node)) {
								context.report({
									node,
									message: 'Each import in a multiline import should be on its own line',
									fix(fixer) {
										return fixerMultiline(node, fixer);
									},
								});
							}
						}
						break;
					}
				}
			},
		};
	},
};
