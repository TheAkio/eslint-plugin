import importDeclarationMultiline from '../rules/import-declaration-multiline.js';
import noMarkedTests from '../rules/no-marked-tests.js';

export default {
	'import-declaration-multiline': importDeclarationMultiline,
	'no-marked-tests': noMarkedTests,
};
