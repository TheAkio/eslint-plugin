import config from './config/index.js';
import plugin from './plugin/index.js';

export default {
	...plugin,
	configs: config.reduce((obj, entry) => {
		obj[entry.name] = [
			{
				ignores: [
					'node_modules/**',
					'dist/**',
					'build/**',
					'.angular/**',
				],
			},
			{
				plugins: {
					'@akio': plugin,
				},
			},
			...entry.config.map((flatConfig, index) => ({
				...flatConfig,
				name: `@akio/${entry.name}/${flatConfig.name ?? index}`,
			})),
		];
		return obj;
	}, {}),
};
