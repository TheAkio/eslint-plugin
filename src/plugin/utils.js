import typescriptEslint from 'typescript-eslint';

export function applyJsLikeFiles(configs = [{}]) {
	return configs.map(config => ({
		...config,
		files: [
			'**/*.js',
			'**/*.cjs',
			'**/*.mjs',
			'**/*.jsx',
			'**/*.ts',
			'**/*.tsx',
		],
	}));
}

export function applyTypeScript(configs = [{}]) {
	return configs.map(config => ({
		...config,
		files: [
			'**/*.ts',
			'**/*.tsx',
		],
		languageOptions: {
			...(config.languageOptions || {}),
			ecmaVersion: 'latest',
			sourceType: 'module',
			parser: typescriptEslint.parser,
			parserOptions: {
				projectService: true,
				tsconfigRootDir: '.',
			},
		},
	}));
}

export function applyImportPluginSettings(configs = [{}]) {
	return configs.map(config => ({
		...config,
		settings: {
			'import/parsers': {
				'@typescript-eslint/parser': [
					'.ts',
					'.tsx',
				],
			},
			'import/resolver': {
				typescript: {
					alwaysTryTypes: true,
					project: './tsconfig.json',
				},
			},
		},
	}));
};
