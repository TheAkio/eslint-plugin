// ESM syntax for importing JSON that is not fully supported yet
// import _package from '../../package.json' assert { type: 'json' };

import rules from '../rules/index.js';

// Old way of importing JSON files
import { resolve } from 'node:path';
import { readFileSync } from 'node:fs';
const _package = JSON.parse(readFileSync(resolve(import.meta.dirname, '../../package.json'), { encoding: 'utf8' }));

export default {
	meta: { name: _package.name, version: _package.version },
	rules,
};
