/**
 * ESLint rules from this package
 */
export default [
	{
		rules: {
			'@akio/import-declaration-multiline': [
				'error',
				'as-needed',
				{
					maxLengthSingelineImport: 140,
				},
			],
		},
	},
];
