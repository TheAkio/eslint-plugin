import typescriptEslint from 'typescript-eslint';
import stylisticTs from '@stylistic/eslint-plugin-ts';

// TODO: Rules from @stylistic/ts should be put into a separate file
/**
 * Linting rules for TypeScript.
 *
 * @see https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/ROADMAP.md
 */
export default [
	typescriptEslint.configs.eslintRecommended,
	...typescriptEslint.configs.recommended,
	...typescriptEslint.configs.recommendedTypeChecked,
	{
		plugins: {
			'@stylistic/ts': stylisticTs,
		},
		rules: {
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/array-type.md
			// TODO: Consider enabling this rule
			'@typescript-eslint/array-type': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/comma-dangle.md
			'@stylistic/ts/comma-dangle': [
				'error',
				'always-multiline',
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/comma-spacing.md
			'@stylistic/ts/comma-spacing': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-assertions.md
			'@typescript-eslint/consistent-type-assertions': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-definitions.md
			'@typescript-eslint/consistent-type-definitions': [
				'error',
				'interface',
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/dot-notation.md
			'@typescript-eslint/dot-notation': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-module-boundary-types.md
			'@typescript-eslint/explicit-module-boundary-types': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/indent.md
			'@stylistic/ts/indent': [
				'error',
				'tab',
				{
					SwitchCase: 1,
					flatTernaryExpressions: true,
					FunctionDeclaration: {
						parameters: 'first',
					},
					FunctionExpression: {
						parameters: 'first',
					},
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/keyword-spacing.md
			'@stylistic/ts/keyword-spacing': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/lines-between-class-members.md
			'@stylistic/ts/lines-between-class-members': [
				'error',
				'always',
				{
					exceptAfterSingleLine: true,
					exceptAfterOverload: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-delimiter-style.md
			'@stylistic/ts/member-delimiter-style': [
				'error',
				{
					overrides: {
						interface: {
							multiline: {
								delimiter: 'semi',
								requireLast: true,
							},
							singleline: {
								delimiter: 'semi',
								requireLast: false,
							},
						},
						typeLiteral: {
							multiline: {
								delimiter: 'comma',
								requireLast: true,
							},
							singleline: {
								delimiter: 'comma',
								requireLast: false,
							},
						},
					},
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-ordering.md
			'@typescript-eslint/member-ordering': [
				'warn',
				{
					default: [
						'signature',

						'static-field',
						'instance-field',
						'abstract-field',

						'constructor',

						'static-method',
						'instance-method',
						'abstract-method',
					],
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/naming-convention.md
			'@typescript-eslint/naming-convention': [
				'warn',
				{
					selector: 'default',
					format: ['camelCase'],
					leadingUnderscore: 'allow',
					trailingUnderscore: 'forbid',
				},

				{
					selector: 'variableLike',
					format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
					leadingUnderscore: 'allow',
					trailingUnderscore: 'forbid',
				},
				{
					selector: 'memberLike',
					format: ['camelCase', 'UPPER_CASE'],
					leadingUnderscore: 'allow',
					trailingUnderscore: 'forbid',
				},
				{
					selector: 'typeLike',
					format: ['PascalCase'],
				},

				// Function names should regularly be in camelCase but using UPPER_CASE is also allowed
				{
					selector: 'function',
					format: ['camelCase', 'UPPER_CASE'],
				},
				// Enforce all enum members to be UPPER_CASE
				{
					selector: 'enumMember',
					format: ['UPPER_CASE'],
				},
				// Special PascalCase exception for readonly attributes in Angular components
				// that make, for example, an enum available for the template
				{
					selector: 'classProperty',
					modifiers: ['readonly'],
					format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
				},
			],
			// https://typescript-eslint.io/rules/no-deprecated/
			'@typescript-eslint/no-deprecated': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-empty-function.md
			'@typescript-eslint/no-empty-function': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-empty-interface.md
			'@typescript-eslint/no-empty-interface': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-explicit-any.md
			'@typescript-eslint/no-explicit-any': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-extra-semi.md
			'@stylistic/ts/no-extra-semi': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-floating-promises.md
			'@typescript-eslint/no-floating-promises': [
				'error',
				{
					ignoreVoid: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-inferrable-types.md
			'@typescript-eslint/no-inferrable-types': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-namespace.md
			'@typescript-eslint/no-namespace': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-non-null-assertion.md
			'@typescript-eslint/no-non-null-assertion': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-redeclare.md
			// Rule should remain disabled. Reason: https://github.com/typescript-eslint/typescript-eslint/issues/2585#issuecomment-696331548
			'@typescript-eslint/no-redeclare': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-shadow.md
			'@typescript-eslint/no-shadow': [
				'warn',
				{
					hoist: 'all',
					ignoreTypeValueShadow: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-this-alias.md
			'@typescript-eslint/no-this-alias': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-boolean-literal-compare.md
			'@typescript-eslint/no-unnecessary-boolean-literal-compare': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-type-assertion.md
			'@typescript-eslint/no-unnecessary-type-assertion': 'warn',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-expressions.md
			'@typescript-eslint/no-unused-expressions': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-vars.md
			'@typescript-eslint/no-unused-vars': [
				'warn',
				{
					args: 'all',
					argsIgnorePattern: '^_',
					caughtErrors: 'all',
					caughtErrorsIgnorePattern: '^_',
					destructuredArrayIgnorePattern: '^_',
					varsIgnorePattern: '^_',
					ignoreRestSiblings: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/object-curly-spacing.md
			'@stylistic/ts/object-curly-spacing': [
				'error',
				'always',
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-for-of.md
			'@typescript-eslint/prefer-for-of': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-function-type.md
			'@typescript-eslint/prefer-function-type': 'error',
			// TODO: Consider enabling this rule (is the compiler handling this properly when going to ES5?)
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-includes.md
			'@typescript-eslint/prefer-includes': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-readonly.md
			'@typescript-eslint/prefer-readonly': 'error',
			// TODO: Consider enabling this rule (is the compiler handling this properly when going to ES5?)
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-string-starts-ends-with.md
			'@typescript-eslint/prefer-string-starts-ends-with': 'off',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/promise-function-async.md
			'@typescript-eslint/promise-function-async': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/quotes.md
			'@stylistic/ts/quotes': [
				'error',
				'single',
			],
			// TODO: This rule seems buggy, eventually enable it later
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/restrict-template-expressions.md
			'@typescript-eslint/restrict-template-expressions': [
				'off',
				{
					allowNumber: true,
					allowBoolean: true,
					allowAny: true,
					allowNullable: false,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/semi.md
			'@stylistic/ts/semi': [
				'error',
				'always',
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/space-before-function-paren.md
			'@stylistic/ts/space-before-function-paren': [
				'error',
				{
					anonymous: 'never',
					named: 'never',
					asyncArrow: 'always',
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/type-annotation-spacing.md
			'@stylistic/ts/type-annotation-spacing': 'error',
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unbound-method.md
			'@typescript-eslint/unbound-method': [
				'warn',
				{
					ignoreStatic: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unified-signatures.md
			'@typescript-eslint/unified-signatures': 'error',
		},
	},
];
