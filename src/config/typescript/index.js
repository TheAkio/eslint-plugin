import { applyTypeScript } from '../../plugin/utils.js';

import jsConfig from '../javascript/index.js';

import eslintOverrideConfig from './eslint.js';
import typescriptEslintConfig from './typescript-eslint.js';
import eslintImportConfig from './eslint-plugin-import.js';
import akiolintConfig from './akiolint.js';

/**
 * Configuration for linting TypeScript.
 * This is also includes the JavaScript base configuration.
 */
export default [
	// applyTypescript is not ran for jsConfig for it to remain as-is
	...jsConfig,
	...applyTypeScript(eslintOverrideConfig),
	...applyTypeScript(typescriptEslintConfig),
	...applyTypeScript(eslintImportConfig),
	...applyTypeScript(akiolintConfig),
	{
		files: [
			'**/*.spec.ts',
		],
		rules: {
			// With strict-mode you sometimes just want to pass undefined instead of mocking something the function you call
			// doesn't even need. So we allow non-null-assertions in test files
			'@typescript-eslint/no-non-null-assertion': 'off',
			// Enable a rule to warn for xdescribe, xit, fdescribe and fit
			'@akio/no-marked-tests': 'warn',
		},
	},
];
