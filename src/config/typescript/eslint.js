/**
 * ESLint rules for TypeScript disabling certain rules inside the JavaScript configuration.
 * Only turn rules off in this file!
 *
 * @see https://eslint.org/docs/rules/
 */
export default [
	{
		rules: {
			// https://eslint.org/docs/rules/comma-dangle
			'comma-dangle': 'off',
			// https://eslint.org/docs/rules/comma-spacing
			'comma-spacing': 'off',
			// https://eslint.org/docs/rules/dot-notation
			'dot-notation': 'off',
			// https://eslint.org/docs/rules/indent
			'indent': 'off',
			// https://eslint.org/docs/rules/keyword-spacing
			'keyword-spacing': 'off',
			// https://eslint.org/docs/rules/no-redeclare
			'no-redeclare': 'off',
			// https://eslint.org/docs/rules/no-shadow
			'no-shadow': 'off',
			// https://eslint.org/docs/rules/no-unused-expressions
			'no-unused-expressions': 'off',
			// https://eslint.org/docs/rules/no-unused-vars
			'no-unused-vars': 'off',
			// https://eslint.org/docs/rules/object-curly-spacing
			'object-curly-spacing': 'off',
			// https://eslint.org/docs/rules/quotes
			'quotes': 'off',
			// https://eslint.org/docs/rules/semi
			'semi': 'off',
			// https://eslint.org/docs/rules/space-before-function-paren
			'space-before-function-paren': 'off',
		},
	},
];
