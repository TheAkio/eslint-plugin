import * as importPlugin from 'eslint-plugin-import';
import { applyImportPluginSettings } from '../../plugin/utils.js';

/**
 * Rules for linting imports in TypeScript.
 *
 * The rule no-internal-modules should be configured per project.
 *
 * @see https://github.com/benmosher/eslint-plugin-import
 */
export default [
	...applyImportPluginSettings([
		importPlugin.flatConfigs.recommended,
		importPlugin.flatConfigs.typescript,
		{
			rules: {
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/extensions.md
				'import/extensions': [
					'error',
					'never',
				],
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-absolute-path.md
				'import/no-absolute-path': 'error',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-amd.md
				'import/no-amd': 'error',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-commonjs.md
				'import/no-commonjs': 'error',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-cycle.md
				'import/no-cycle': [
					'error',
					{
						maxDepth: 3,
					},
				],
				// https://github.com/benmosher/eslint-plugin-import/blob/HEAD/docs/rules/no-deprecated.md
				'import/no-deprecated': 'warn',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-mutable-exports.md
				'import/no-mutable-exports': 'error',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-self-import.md
				'import/no-self-import': 'error',
				// TODO: Enable this rule, make sure it doesn't complain about Angulars main.ts or the main index.ts
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unused-modules.md
				'import/no-unused-modules': 'off',
				// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-useless-path-segments.md
				'import/no-useless-path-segments': [
					'error',
					{
						noUselessIndex: true,
					},
				],
				// https://github.com/benmosher/eslint-plugin-import/blob/HEAD/docs/rules/order.md
				'import/order': [
					'error',
					{
						'groups': ['builtin', 'external', 'parent', 'sibling', 'index'],
						'pathGroups': [
							{
								pattern: '@angular/**/testing',
								group: 'builtin',
								position: 'before',
							},
							{
								pattern: '@angular/**',
								group: 'builtin',
								position: 'after',
							},
						],
						'pathGroupsExcludedImportTypes': ['builtin'],
						'newlines-between': 'always',
						'alphabetize': {
							order: 'asc',
							caseInsensitive: false,
						},
					},
				],
			},
		},
	]),
];
