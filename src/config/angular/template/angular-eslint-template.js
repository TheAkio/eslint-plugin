import angularEslint from 'angular-eslint';

/**
 * Rules from @angular/eslint for linting HTML templates.
 *
 * Rules match the codelyzer name.
 * There is currently no documentation for angular-eslint so the codelyzer documentation is used.
 *
 * @see https://github.com/angular-eslint/angular-eslint
 */
export default [
	...angularEslint.configs.templateRecommended,
	{
		rules: {
			// https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/docs/rules/banana-in-box.md
			'@angular-eslint/template/banana-in-box': 'error',
			// https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/docs/rules/cyclomatic-complexity.md
			'@angular-eslint/template/cyclomatic-complexity': [
				'error',
				{
					maxComplexity: 15,
				},
			],
			// https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/docs/rules/no-any.md
			'@angular-eslint/template/no-any': 'warn',
			// https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/docs/rules/no-negated-async.md
			'@angular-eslint/template/no-negated-async': 'error',
			// https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/docs/rules/eqeqeq.md
			'@angular-eslint/template/eqeqeq': [
				'error',
				{
					allowNullOrUndefined: true,
				},
			],
		},
	},
];
