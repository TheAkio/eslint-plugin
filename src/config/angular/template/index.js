import { applyTypeScript } from '../../../plugin/utils.js';

import angularEslint from 'angular-eslint';
import angularEslintTemplate from './angular-eslint-template.js';

function applyFilesHtml(configs) {
	return configs.map(config => ({
		...config,
		files: [
			'**/*.html',
		],
	}));
}

/**
 * Angular HTML template linting configuration.
 * Also lints inline-templates in TypeScript component files.
 */
export default [
	...applyTypeScript([{}]),
	{
		files: [
			'**/*.component.ts',
		],
		processor: angularEslint.processInlineTemplates,
	},
	...applyFilesHtml(angularEslintTemplate),
];
