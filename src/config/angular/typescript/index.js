import { applyTypeScript } from '../../../plugin/utils.js';

import eslintOverrideConfig from './eslint.js';
import typescriptEslintConfig from './typescript-eslint.js';
import eslintImportConfig from './eslint-plugin-import.js';
import angularEslintConfig from './angular-eslint.js';

/**
 * Angular TypeScript component linting.
 * This configuration does not include the basic TypeScript configuration, it has to be added in addition.
 */
export default [
	...applyTypeScript(eslintOverrideConfig),
	...applyTypeScript(typescriptEslintConfig),
	...applyTypeScript(eslintImportConfig),
	...applyTypeScript(angularEslintConfig),
	{
		files: [
			'**/*.spec.ts',
		],
		rules: {
			// Disable this rule as Angular tests are allowed to manually call lifecycle functions
			'@angular-eslint/no-lifecycle-call': 'off',
		},
	},
];
