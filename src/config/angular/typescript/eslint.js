/**
 * Angular-specific ESLint configuration.
 *
 * @see https://eslint.org/docs/rules/
 */
export default [
	{
		rules: {
			// https://eslint.org/docs/rules/no-restricted-imports
			'no-restricted-imports': [
				'error',
				'rxjs/Rx',
			],
		},
	},
];
