import angularEslint from 'angular-eslint';

/**
 * Rules from @angular/eslint for linting TypeScript components.
 *
 * Rules match the codelyzer name.
 * There is currently no documentation for angular-eslint so the codelyzer documentation is used.
 *
 * The rules component-selector, directive-selector and pipe-prefix should be configured per project.
 *
 * @see https://github.com/angular-eslint/angular-eslint
 */
export default [
	...angularEslint.configs.tsRecommended,
	{
		rules: {
			// http://codelyzer.com/rules/component-class-suffix/
			'@angular-eslint/component-class-suffix': 'error',
			// http://codelyzer.com/rules/component-max-inline-declarations/
			'@angular-eslint/component-max-inline-declarations': [
				'error',
				{
					animations: 15,
					styles: 3,
					template: 12,
				},
			],
			// http://codelyzer.com/rules/contextual-decorator/
			'@angular-eslint/contextual-decorator': 'error',
			// http://codelyzer.com/rules/contextual-lifecycle/
			'@angular-eslint/contextual-lifecycle': 'error',
			// http://codelyzer.com/rules/directive-class-suffix/
			'@angular-eslint/directive-class-suffix': 'error',
			// http://codelyzer.com/rules/no-conflicting-lifecycle/
			'@angular-eslint/no-conflicting-lifecycle': 'warn',
			// http://codelyzer.com/rules/no-input-rename/
			'@angular-eslint/no-input-rename': 'warn',
			// http://codelyzer.com/rules/no-inputs-metadata-property/
			'@angular-eslint/no-inputs-metadata-property': 'error',
			// http://codelyzer.com/rules/no-lifecycle-call/
			'@angular-eslint/no-lifecycle-call': 'error',
			// http://codelyzer.com/rules/no-output-native/
			'@angular-eslint/no-output-native': 'error',
			// http://codelyzer.com/rules/no-output-on-prefix/
			'@angular-eslint/no-output-on-prefix': 'warn',
			// http://codelyzer.com/rules/no-outputs-metadata-property/
			'@angular-eslint/no-outputs-metadata-property': 'error',
			// http://codelyzer.com/rules/no-pipe-impure/
			'@angular-eslint/no-pipe-impure': 'warn',
			// http://codelyzer.com/rules/no-queries-metadata-property/
			'@angular-eslint/no-queries-metadata-property': 'error',
			// TODO: http://codelyzer.com/rules/no-unused-css/
			// http://codelyzer.com/rules/prefer-on-push-component-change-detection/
			'@angular-eslint/prefer-on-push-component-change-detection': 'warn',
			// http://codelyzer.com/rules/prefer-output-readonly/
			'@angular-eslint/prefer-output-readonly': 'warn',
			// http://codelyzer.com/rules/relative-url-prefix/
			'@angular-eslint/relative-url-prefix': 'warn',
			// http://codelyzer.com/rules/use-component-selector/
			'@angular-eslint/use-component-selector': 'error',
			// http://codelyzer.com/rules/use-component-view-encapsulation/
			'@angular-eslint/use-component-view-encapsulation': 'error',
			// This won't work if you have an injectable that can't be provided in root
			// http://codelyzer.com/rules/use-injectable-provided-in/
			'@angular-eslint/use-injectable-provided-in': 'off',
			// http://codelyzer.com/rules/use-lifecycle-interface/
			'@angular-eslint/use-lifecycle-interface': 'error',
			// http://codelyzer.com/rules/use-pipe-transform-interface/
			'@angular-eslint/use-pipe-transform-interface': 'error',
		},
	},
];
