import { applyImportPluginSettings } from '../../../plugin/utils.js';

/**
 * Additional rules for linting imports in Angular projects.
 *
 * @see https://github.com/benmosher/eslint-plugin-import
 */
export default [
	...applyImportPluginSettings([{
		rules: {
			// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-nodejs-modules.md
			'import/no-nodejs-modules': 'error',
		},
	}]),
];
