import stylisticTs from '@stylistic/eslint-plugin-ts';

// TODO: Rules from @stylistic/ts should be put into a separate file
/**
 * Additional rules for linting regular TypeScript in Angular projects.
 *
 * @see https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/ROADMAP.md
 */
export default [
	{
		plugins: {
			'@stylistic/ts': stylisticTs,
		},
		rules: {
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-use-before-define.md
			'@typescript-eslint/no-use-before-define': [
				'error', {
					functions: true,
					// There is currently a bug with this rule that decorators referencing their own classes get flagged
					// See https://github.com/typescript-eslint/typescript-eslint/issues/1856
					classes: false,
					enums: true,
					variables: true,
					typedefs: true,
				},
			],
			// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/indent.md
			'@stylistic/ts/indent': [
				'error',
				'tab',
				{
					SwitchCase: 1,
					flatTernaryExpressions: true,
					FunctionDeclaration: {
						parameters: 'first',
					},
					FunctionExpression: {
						parameters: 'first',
					},
					// Fixes ESLint with TypeScript ESLint marking property definitions with decorators
					// This does disable indentation check completely, but the VSCode Formatter manages them anyway so it should be fine
					// Also this is only done in the Angular ruleset because it uses decorators
					// See https://github.com/eslint/eslint/issues/15299#issuecomment-968099681
					ignoredNodes: ['PropertyDefinition'],
				},
			],
		},
	},
];
