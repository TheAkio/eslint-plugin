import eslintJs from '@eslint/js';

/**
 * ESLint rules for JavaScript.
 *
 * @see https://eslint.org/docs/rules/
 */
export default [
	eslintJs.configs.recommended,
	{
		rules: {
			// https://eslint.org/docs/rules/accessor-pairs
			'accessor-pairs': [
				'error',
				{
					enforceForClassMembers: true,
				},
			],
			// https://eslint.org/docs/rules/array-callback-return
			'array-callback-return': 'error',
			// https://eslint.org/docs/rules/arrow-body-style
			'arrow-body-style': [
				'error',
				'as-needed',
			],
			// https://eslint.org/docs/rules/arrow-parens
			'arrow-parens': [
				'error',
				'as-needed',
			],
			// https://eslint.org/docs/rules/arrow-spacing
			'arrow-spacing': 'error',
			// https://eslint.org/docs/rules/brace-style
			'brace-style': [
				'error',
				'1tbs',
			],
			// https://eslint.org/docs/rules/comma-dangle
			'comma-dangle': [
				'error',
				'always-multiline',
			],
			// https://eslint.org/docs/rules/comma-spacing
			'comma-spacing': 'error',
			// TODO: It probably makes sense to enable some kind of function complexity check
			// https://eslint.org/docs/rules/complexity
			'complexity': 'off',
			// https://eslint.org/docs/rules/curly
			'curly': [
				'error',
				'multi-line',
				'consistent',
			],
			// https://eslint.org/docs/rules/dot-notation
			'dot-notation': 'error',
			// https://eslint.org/docs/rules/eol-last
			'eol-last': 'error',
			// https://eslint.org/docs/rules/eqeqeq
			'eqeqeq': [
				'error',
				'always',
				{
					null: 'ignore',
				},
			],
			// https://eslint.org/docs/rules/guard-for-in
			'guard-for-in': 'error',
			// https://eslint.org/docs/rules/id-blacklist
			'id-blacklist': [
				'error',
				'Number',
				'number',
				'String',
				'string',
				'Boolean',
				'boolean',
				'Undefined',
			],
			// https://eslint.org/docs/rules/id-match
			'id-match': 'error',
			// https://eslint.org/docs/rules/indent
			'indent': [
				'warn',
				'tab',
				{
					SwitchCase: 1,
					flatTernaryExpressions: true,
					FunctionDeclaration: {
						parameters: 'first',
					},
					FunctionExpression: {
						parameters: 'first',
					},
				},
			],
			// https://eslint.org/docs/rules/key-spacing
			'key-spacing': 'error',
			// https://eslint.org/docs/rules/keyword-spacing
			'keyword-spacing': 'error',
			// https://eslint.org/docs/rules/max-classes-per-file
			'max-classes-per-file': 'off',
			// https://eslint.org/docs/rules/max-len
			'max-len': [
				'warn',
				{
					code: 140,
					ignoreUrls: true,
					ignoreTemplateLiterals: true,
				},
			],
			// https://eslint.org/docs/rules/new-parens
			'new-parens': 'error',
			// https://eslint.org/docs/rules/no-bitwise
			'no-bitwise': 'error',
			// https://eslint.org/docs/rules/no-caller
			'no-caller': 'error',
			// https://eslint.org/docs/rules/no-console
			'no-console': 'warn',
			// https://eslint.org/docs/rules/no-constant-condition
			'no-constant-condition': [
				'error',
				{ checkLoops: false },
			],
			// https://eslint.org/docs/rules/no-else-return
			'no-else-return': 'error',
			// https://eslint.org/docs/rules/no-empty
			'no-empty': 'warn',
			// https://eslint.org/docs/rules/no-eval
			'no-eval': 'error',
			// TODO: Consider enabling this rule (there is a @typescript-eslint equivalent for this rule)
			// https://eslint.org/docs/rules/no-extra-parens
			'no-extra-parens': 'off',
			// https://eslint.org/docs/rules/no-fallthrough
			'no-fallthrough': 'off',
			// TODO: Check if it's possible to enable this rule (there is a @typescript-eslint equivalent for this rule)
			// https://eslint.org/docs/rules/no-invalid-this
			'no-invalid-this': 'off',
			// https://eslint.org/docs/rules/no-labels
			'no-labels': 'error',
			// https://eslint.org/docs/rules/no-multiple-empty-lines
			'no-multiple-empty-lines': [
				'warn',
				{
					max: 1,
				},
			],
			// https://eslint.org/docs/rules/no-new-wrappers
			'no-new-wrappers': 'error',
			// https://eslint.org/docs/rules/no-return-await
			'no-return-await': 'error',
			// https://eslint.org/docs/rules/no-shadow
			'no-shadow': [
				'warn',
				{
					hoist: 'all',
				},
			],
			// https://eslint.org/docs/rules/no-restricted-syntax
			'no-restricted-syntax': [
				'error',
				{
					selector: 'SequenceExpression',
					message: 'Using the comma operator is not allowed.',
				},
			],
			// https://eslint.org/docs/rules/no-template-curly-in-string
			'no-template-curly-in-string': 'error',
			// https://eslint.org/docs/rules/no-throw-literal
			'no-throw-literal': 'error',
			// https://eslint.org/docs/rules/no-trailing-spaces
			'no-trailing-spaces': 'error',
			// https://eslint.org/docs/rules/no-undef-init
			'no-undef-init': 'error',
			// https://eslint.org/docs/rules/no-underscore-dangle
			'no-underscore-dangle': 'off',
			// https://eslint.org/docs/rules/no-unused-expressions
			'no-unused-expressions': 'error',
			// https://eslint.org/docs/rules/no-unused-vars
			'no-unused-vars': 'warn',
			// https://eslint.org/docs/rules/no-warning-comments
			'no-warning-comments': [
				'warn',
				{
					terms: ['todo', 'fixme'],
					location: 'start',
				},
			],
			// https://eslint.org/docs/rules/object-curly-spacing
			'object-curly-spacing': [
				'error',
				'always',
			],
			// https://eslint.org/docs/rules/object-shorthand
			'object-shorthand': 'error',
			// https://eslint.org/docs/rules/one-var
			'one-var': [
				'error',
				'never',
			],
			// https://eslint.org/docs/rules/padded-blocks
			'padded-blocks': [
				'error',
				{
					classes: 'always',
					blocks: 'never',
					switches: 'never',
				},
			],
			// TODO: Consider enabling this rule
			// https://eslint.org/docs/rules/padding-line-between-statements
			'padding-line-between-statements': [
				'off',
				{
					blankLine: 'always',
					prev: '*',
					next: 'return',
				},
			],
			// https://eslint.org/docs/rules/prefer-const
			'prefer-const': [
				'error',
				{
					destructuring: 'all',
					ignoreReadBeforeAssign: true,
				},
			],
			// https://eslint.org/docs/rules/prefer-template
			'prefer-template': 'error',
			// https://eslint.org/docs/rules/quote-props
			'quote-props': [
				'error',
				'consistent-as-needed',
			],
			// https://eslint.org/docs/rules/quotes
			'quotes': [
				'error',
				'single',
			],
			// https://eslint.org/docs/rules/radix
			'radix': 'error',
			// https://eslint.org/docs/rules/rest-spread-spacing
			'rest-spread-spacing': 'error',
			// TODO: This rule seems to have some false positives or something, need to find out further what exactly it does
			// https://eslint.org/docs/rules/require-atomic-updates
			'require-atomic-updates': 'off',
			// https://eslint.org/docs/rules/semi
			'semi': 'error',
			// https://eslint.org/docs/rules/semi-spacing
			'semi-spacing': 'error',
			// https://eslint.org/docs/rules/spaced-comment
			'spaced-comment': 'error',
			// https://eslint.org/docs/rules/space-before-blocks
			'space-before-blocks': 'error',
			// https://eslint.org/docs/rules/space-before-function-paren
			'space-before-function-paren': [
				'error',
				{
					anonymous: 'never',
					named: 'never',
					asyncArrow: 'always',
				},
			],
			// https://eslint.org/docs/rules/space-in-parens
			'space-in-parens': [
				'error',
				'never',
			],
			// https://eslint.org/docs/rules/space-infix-ops
			'space-infix-ops': 'error',
			// https://eslint.org/docs/rules/yoda
			'yoda': 'error',
		},
	},
];
