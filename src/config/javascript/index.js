import globals from 'globals';

import jsEslintConfig from './eslint.js';
import { applyJsLikeFiles } from '../../plugin/utils.js';

/**
 * Configuration for linting JavaScript.
 * This is also used inside the TypeScript configuration as a base.
 */
export default [
	{
		files: [
			'**/webpack*.js',
			'**/karma.conf.js',
			'**/eslint.config.js',
			'**/proxy-conf*.js',
		],
		languageOptions: {
			ecmaVersion: 'latest',
			globals: {
				...globals.node,
			},
		},
	},
	...applyJsLikeFiles(jsEslintConfig),
];
