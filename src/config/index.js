import javascript from './javascript/index.js';
import typescript from './typescript/index.js';
import angularTypescript from './angular/typescript/index.js';
import angularTemplate from './angular/template/index.js';

// TODO: Missing rule: TSLint/static-this -> See https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/ROADMAP.md
// TODO: Missing rule: JSDoc (valid-jsdoc, etc.) -> See Roadmap above

// TODO: Missing ESLint rules but formatted by VSCode:
// Rule for union type spacing; "type Type = 'abc'|'def'" is allowed
// Rule for class attribute declaration spacing; "readonly attribute=0" is allowed
export default [
	{
		name: 'javascript',
		config: javascript,
	},
	{
		name: 'typescript',
		config: typescript,
	},
	{
		name: 'angularTypescript',
		config: angularTypescript,
	},
	{
		name: 'angularTemplate',
		config: angularTemplate,
	},
];
