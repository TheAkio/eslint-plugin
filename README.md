# @akio/eslint-plugin [![NPM](https://img.shields.io/npm/v/@akio/eslint-plugin.svg)](https://www.npmjs.com/package/@akio/eslint-plugin) [![MIT License](https://img.shields.io/npm/l/@akio/eslint-plugin)](https://www.npmjs.com/package/@akio/eslint-plugin) [![Dependencies](https://img.shields.io/librariesio/release/npm/@akio/eslint-plugin)](https://www.npmjs.com/package/@akio/eslint-plugin?activeTab=dependencies)

Personal ESLint plugin for JavaScript, TypeScript and Angular with mainly configuration and some self-written rules. Feel free to suggest changes but I'll be keeping my tabs :)

## Installation

Install via npm:
```bash
npm i --save-dev @akio/eslint-plugin
```

## Setup

Create your `eslint.config.mjs` (Or `eslint.config.js` if `"type": "module"` is set in your `package.json`) file and configure it according to the steps in the **Configuration** section.

## Updating

To update this package, follow all the steps of the installation guide again, except configuring the `eslint.config.mjs` file.

## Configuration

Put the `eslint.config.mjs` file into the root folder of your project.
Some default globals may be given, but they should be configured according to <https://eslint.org/docs/latest/use/configure/language-options#predefined-global-variables>

### For JavaScript linting

`eslint.config.mjs`:

```js
import globals from 'globals';
import akiolint from '@akio/eslint-plugin';

export default [
    ...akiolint.configs.javascript,
    {
        languageOptions: {
            ecmaVersion: 'latest',
            globals: {
                ...globals.browser,
            },
        },
    },
];
```

### For TypeScript linting

`eslint.config.mjs`:

```js
import globals from 'globals';
import akiolint from '@akio/eslint-plugin';

export default [
    ...akiolint.configs.typescript,
    {
        languageOptions: {
            ecmaVersion: 'latest',
            globals: {
                ...globals.browser,
            },
        },
    },
];
```

### For Angular linting

`eslint.config.mjs`:

```js
import globals from 'globals';
import akiolint from '@akio/eslint-plugin';

export default [
    ...akiolint.configs.typescript,
    ...akiolint.configs.angularTypescript,
    ...akiolint.configs.angularTemplate,
    {
        languageOptions: {
            ecmaVersion: 'latest',
            globals: {
                ...globals.browser,
            },
        },
    },
    {
        files: [
            '**/*.ts',
        ],
        rules: {
            '@angular-eslint/component-selector': [
                'error',
                {
                    type: 'element',
                    prefix: /* Replace with prefix for components */,
                    style: 'kebab-case',
                },
            ],
            '@angular-eslint/directive-selector': [
                'error',
                {
                    type: 'attribute',
                    prefix: /* Replace with prefix for components */,
                    style: 'camelCase',
                },
            ],
        },
    },
];
```

## Migrate from V1

ESLint 9 threw everything ESLint 8 did out of the window and basically created a completely different linting infrastructure.
All used plugins basically did the same, and so Akiolint was kind of forced to do the same.
**The easiest way to migrate from V1 is probably to just setup Akiolint again.**

To remove Akiolint V1, remove the following things from your `package.json`:

- The `update-akiolint` script
- The `@akio/eslint-plugin` dependency
- All other dependencies with `eslint` in their name (`@angular-eslint/...`, `@typescript-eslint/...`, etc.)

Also remove all ESLint configuration files (like `.eslintrc.json` or `.eslintrc.js`) from your project.

Finally remove your `package-lock.json` and `node_modules` and then run `npm i`.

Now you can start with the **Installation** section above.
