import globals from 'globals';

import jsConfig from './src/config/javascript/index.js';

export default [
	...jsConfig,
	{
		rules: {
			'no-warning-comments': 'off',
		},
		languageOptions: {
			ecmaVersion: 'latest',
			globals: {
				...globals.node,
			},
		},
	},
];
